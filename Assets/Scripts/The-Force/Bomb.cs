﻿using UnityEngine;
using System.Collections;

namespace TheForce {
    public class Bomb : Damager {

        public float delay = 2;
        public float radius = 2;

        // Use this for initialization
        void Start() {
            Invoke("Explode", delay);
        }

        void Explode() {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius);
            foreach(Collider2D collider in colliders){
                DoDamageTo(collider.gameObject);
            }
            Destroy(gameObject);
        }

        
    }
}
