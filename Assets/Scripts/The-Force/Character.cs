﻿using UnityEngine;
using System.Collections.Generic;

namespace TheForce {

    /// <summary> 
    /// Basic class for objects with life and damage resistances
    /// </summary> 
    public class Character : 
        MonoBehaviour, IDamageable, IKillable, IEnumerable<Damage> {

        // Character's life
        protected float life      = 0;
        // Character's maximum Life
        public float maxLife   = 100;


        // Damage Resistances List
        [SerializeField]
        protected List<Damage> resistances = new List<Damage>();


        /// <summary> 
        /// Property for accesing life. It ensures that life is never 
        /// less than 0 and more than maxLife
        /// </summary> 
        public virtual float Life {
            get { return life; }
            set {
                life = value;
                if (life > maxLife) {
                    life = maxLife;
                } else if (life < 0) {
                    life = 0;
                }
            }
        }

        /// <summary> 
        /// Initiates character
        /// </summary> 
        protected void Awake() {
            Life = maxLife;
        }

        /// <summary> 
        /// Called every frame while the Behaviour is active
        /// </summary> 
        protected void Update() {
            // Check Alive
            if (life<= 0) {
                Die();
            }
        }

        /// <summary> 
        /// Called by another object to apply some damage
        /// </summary> 
        /// <param name="damager">
        /// the object that contains damage info and a reference to the damage 
        /// dealer
        /// </param> 
        public virtual void Damage(Damager damager) {

            //Character guyAtackingMe = damager.Owner;

            float resistance;
            float finalDamage;

            foreach (Damage damage in damager) {

                resistance = GetResistance(damage.type);

                try {
                    finalDamage = DamageFormulas.GetFormula(damage.type)(damage.value, resistance);
                } catch {
                    finalDamage = 0;
                }

                life -= finalDamage;
            }
        }


        /// <summary> 
        /// Method to kill the character
        /// </summary> 
        public virtual void Kill() {
            Life = 0;
        }

        /// <summary> 
        /// What happens when the character Dies 
        /// </summary> 
        protected virtual void Die() {
            // TODO
            Destroy(gameObject);
        }



        /// <summary> 
        /// Property for accessing the list of damage resistances
        /// </summary> 
        public List<Damage> Resistances {
            get { return resistances; }
            set { resistances = value; }
        }

        /// <summary> 
        /// Returns the index of the first occurrence of a damage in the damage
        /// resistances list. Returns -1 if not found.
        /// </summary> 
        /// <param name="damageType">
        /// The DamageType that we are searching.
        /// </param>       
        protected int indexOf(DamageType damageType) {
            return resistances.FindIndex(d => d.type == damageType);
        }

        /// <summary> 
        /// Adds or modify a damage in the damage resistances list
        /// </summary> 
        /// <param name="damageType">
        /// The DamageType that we are adding.
        /// </param>
        /// <param name="resistanceValue">
        /// The damage resitance value.
        /// </param>
        public void SetResistance(DamageType damageType, float resistanceValue) {
            int index = indexOf(damageType);
            if (index != -1) {
                resistances[index].value = resistanceValue;
            } else {
                resistances.Add(new Damage(damageType, resistanceValue));
            }
        }

        /// <summary> 
        /// Returns the damage resistance value of a especific damagetype.
        /// Returns 0 if not set.
        /// </summary> 
        /// <param name="damageType">
        /// The DamageType that we are searching.
        /// </param> 
        public float GetResistance(DamageType damageType) {
            int index = indexOf(damageType);
            if (index != -1) {
                return resistances[index].value;
            }
            return 0;
        }

        /// <summary> 
        /// Removes a damage resistance from the damage resistances list.
        /// resistance. Returns 0 if not set.
        /// </summary> 
        /// <param name="damageType">
        /// The DamageType that is going to be removed.
        /// </param> 
        public void RemoveResistance(DamageType damageType) {
            int index = indexOf(damageType);
            if (index != -1) {
                resistances.RemoveAt(index);
            }
        }

        /// <summary> 
        /// Clears the damage resisntaces list.
        /// </summary> 
        public void ClearDamages() {
            resistances.Clear();
        }

        /// <summary> 
        /// Implementation of IEnumerable<Damage> to be able to iterate through
        /// this object as a damage resistances list
        /// </summary> 
        public IEnumerator<Damage> GetEnumerator() {
            foreach (Damage damage in resistances) {
                yield return damage;
            }
        }

        /// <summary> 
        /// Implementation of IEnumerable<Damage> to be able to iterate through
        /// this object as a damage resistances list
        /// </summary> 
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
    }
}
