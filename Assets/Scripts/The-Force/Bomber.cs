﻿using UnityEngine;
using System.Collections;

namespace TheForce {
    public class Bomber : Weapon {

        // Bullet prefab
        public Transform bombPrefab;
        // The distance from the player at wich the bomb will be spawned;
        public float bombSpawnDistance = 0.5f;

        public float maxAmmo = 5;
        private float ammo = 1;

        public float Ammo {
            set {
                ammo = value;
                if (ammo < 0) {
                    ammo = 0;
                } else if (ammo > maxAmmo) {
                    ammo = maxAmmo;
                }
            }
            get { return ammo; }
        }

        /// <summary> 
        /// Initiates the weapon checking needs.
        /// </summary> 
        void Awake() {
            if (bombPrefab == null) {
                Debug.Log("Bomber: No bombPrefab");
                Destroy(gameObject);
            }
        }

        /// <summary> 
        /// Called when player pulls the trigger of the weapon to start charging
        /// </summary> 
        public override void PullTrigger() {
        }

        /// <summary> 
        /// Called when player releases the trigger of the weapon to stop charging and shoot
        /// </summary> 
        public override void ReleaseTrigger() {
            Shoot();
        }

        /// <summary> 
        /// Shoots a bullet
        /// </summary> 
        protected override void Shoot() {
            Transform t     = Instantiate(bombPrefab) as Transform;
            t.position      = transform.position + transform.right * bombSpawnDistance;

            Bomb bullet = t.GetComponent<Bomb>();
            bullet.Owner = Owner;
        }
    }
}
