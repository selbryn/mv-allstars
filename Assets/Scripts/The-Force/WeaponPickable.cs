﻿using UnityEngine;
using System.Collections;

namespace TheForce {
    public class WeaponPickable : Pickable {

        [SerializeField]
        protected GameObject weapon;

        protected override object PickableObject {
            get {
                return weapon;
            }
        }
    }
}
