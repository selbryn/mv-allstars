﻿using UnityEngine;
using System.Collections;

namespace TheForce {

    public class LifePickable : Pickable {

        [SerializeField]
        protected float life;

        protected override object PickableObject {
            get {
                return life;
            }
        }
    }
}