﻿using UnityEngine;
using System.Collections.Generic;

namespace TheForce {

    /// <remarks> 
    /// Enum with damage types.
    /// </remarks> 
    public enum DamageType {
        Basico,
        Fuego,
        Frío,
        Eléctrico,
        Cortante,
        Contundente,
        Perforante,
        Oscuridad,
        Luz,
        Caos,
        Ley,
        Desintegracion_molecular,
        Stasis
    }

    /// <remarks> 
    /// Class for define a damage value and its damage type.
    /// </remarks> 
    [System.Serializable]
    public class Damage {

        // damage type
        public DamageType type;

        // damage value
        public float value;

        /// <summary> 
        /// Class Constructor
        /// </summary> 
        /// <param name="t">
        /// the damage type
        /// </param> 
        /// <param name="v">
        /// the damage value
        /// </param> 
        public Damage(DamageType t, float v) {
            type = t;
            value = v;
        }
    }

    /// <remarks> 
    /// Class with damage formulas for each damagetype
    /// </remarks> 
    public static class DamageFormulas{

        /// <remarks> 
        /// Delegate for all damage formulas.
        /// </remarks>
        /// <param name="damage">
        /// the damage received from damager
        /// </param> 
        /// <param name="damage">
        /// the resistance of the character
        /// </param>  
        public delegate float DamageFormulaDelegate(float damage, float resistance);

        /// <summary> 
        /// Dictionary with all abailable DamageFormulas sorted by its damagetype
        /// </summary>
        public static Dictionary<DamageType, DamageFormulaDelegate> formulas = new Dictionary<DamageType, DamageFormulaDelegate>(){
            {DamageType.Basico, Basic}
        };

        /// <summary> 
        /// Function to get the damage formula of a specific damagetype
        /// </summary>
        /// <param name="damageType">
        /// the damage type of the damageformula.
        /// </param> 
        public static DamageFormulaDelegate GetFormula(DamageType damageType) {
            try {
                return formulas[damageType];
            } catch {
                return null;
            }
        }

        /// <summary> 
        /// Damage formula of Basic damage type
        /// </summary>
        public static float Basic(float damage, float resistance) {
            float finalDamage = damage - resistance;
            finalDamage = finalDamage < 0? 0 : finalDamage;
            return finalDamage;
        }
    }
}
