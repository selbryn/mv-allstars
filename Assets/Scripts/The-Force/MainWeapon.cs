﻿using UnityEngine;
using System.Collections;

namespace TheForce {

    /// <remarks> 
    /// Player main weapon class.
    /// </remarks> 
    public class MainWeapon : Weapon {

        // Bullet prefab
        public Transform bulletPrefab;
        // The distance from the playerat wich the bullet will be spawned;
        public float bulletSpawnDistance = 0.5f;

        // Maximun charge of the weapon.
        public float maxCharge  = 1;
        // charge increment in one second.
        public float chargeRate = 0.5f;

        // is charging.
        private bool charging   = false;
        // is charged.
        private bool charged    = false;
        // Actual charge value.
        private float charge    = 0;


        /// <summary> 
        /// Initiates the weapon checking needs.
        /// </summary> 
        void Awake() {
            if (bulletPrefab == null) {
                Debug.Log("MainWeapon: No bulletPrefab");
                Destroy(gameObject);
            }
        }

        /// <summary> 
        /// called every frame to update weapon state;
        /// </summary> 
        void Update() {
            if (charging) {
                if (!charged) {
                    Charge();
                } else {
                    // Sobrecarga ?
                }
            }
        }

        /// <summary> 
        /// Called when player pulls the trigger of the weapon to start charging
        /// </summary> 
        public override void PullTrigger() {
            if (!charging) {
                charging = true;
            }
        }

        /// <summary> 
        /// Called when player releases the trigger of the weapon to stop charging and shoot
        /// </summary> 
        public override void ReleaseTrigger() {
            charging = false;
            Shoot();
        }

        /// <summary> 
        /// This must be called from Update every frame if chargin
        /// Charges the weapon.
        /// </summary> 
        private void Charge() {
            charge += chargeRate * Time.fixedDeltaTime;
            if (charge >= maxCharge) {
                charge  = maxCharge;
                charged = true;
            }
        }

        /// <summary> 
        /// Shoots a bullet
        /// </summary> 
        protected override void Shoot() {
            Transform t     = Instantiate(bulletPrefab) as Transform;
            t.position      = transform.position + transform.right*bulletSpawnDistance;

            Bullet bullet = t.GetComponent<Bullet>();
            float basicDamage = bullet.GetDamage(DamageType.Basico);
            basicDamage +=  basicDamage * charge/maxCharge;
            bullet.SetDamage(DamageType.Basico, basicDamage);
            bullet.Owner = Owner;
            bullet.initialDirection = transform.right;
            bullet.initialSpeed     += bullet.initialSpeed * charge/maxCharge;

            charged         = false;
            charge          = 0;
        }

    }
}
