﻿using UnityEngine;
using System.Collections;

namespace TheForce {

    /// <remarks> 
    /// Basic class for weapons.
    /// </remarks> 
    public abstract class Weapon : MonoBehaviour, ITriggerable {

        // who is the owner of ths weapon
        protected Character owner;

        /// <summary> 
        /// Property for accesing the owner of this weapon
        /// </summary> 
        public virtual Character Owner {
            get { return owner; }
            set { owner = value; }
        }

        /// <summary> 
        /// ITriggerable implementation
        /// </summary> 
        public virtual void PullTrigger() { }

        /// <summary> 
        /// ITriggerable implementation
        /// </summary> 
        public virtual void ReleaseTrigger() { }

        /// <summary> 
        /// ITriggerable implementation of an instant trigger action.
        /// </summary> 
        public virtual void Trigger() {
            PullTrigger();
            ReleaseTrigger();
        }

        /// <summary> 
        /// Function to shoot the weapon
        /// </summary> 
        protected abstract void Shoot();

    }
}
