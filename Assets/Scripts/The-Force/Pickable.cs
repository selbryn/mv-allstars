﻿using UnityEngine;
using System.Collections;

namespace TheForce {

    public enum PickableType { life, ammo , weapon}

    [RequireComponent(typeof(CircleCollider2D))]
    public abstract class Pickable : MonoBehaviour {

        public PickableType type;

        protected virtual object PickableObject {
            get;
            set;
        }

        void OnTriggerEnter2D(Collider2D collider) {
            IPicker picker = (IPicker)collider.GetComponent(typeof(IPicker));
            if (picker != null) {
                picker.Pick(type, PickableObject);
                Destroy(gameObject);
            }
        }
    }
}