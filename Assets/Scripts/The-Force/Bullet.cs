﻿using UnityEngine;
using System.Collections;

namespace TheForce {
	
	/// <remarks> 
	/// Basic class for bullet-like objects that move, collides and deal damage.
	/// </remarks> 
	[RequireComponent(typeof(Rigidbody2D))]
	[RequireComponent(typeof(CircleCollider2D))]
	public class Bullet : Damager {
		
		// radius of the circle collider 2D
		public float radius             = 0.05f;
		// initial speed of the bullet at spawn
		public float initialSpeed       = 10;
		// lifespan of the bullet before destroying its self
		public float lifespan           = 3;
		// Initial direction of the bullet at spawn
		public Vector2 initialDirection = Vector2.right;
		// Is the bullet destroyed on hit?
		public bool destroyOnHit        = true;
		
		/// <summary> 
		/// Initiates Damager
		/// </summary> 
		void Start() {
			CircleCollider2D circleCollider2D   = (CircleCollider2D)collider2D;
			circleCollider2D.radius             = radius;
			
			rigidbody2D.AddForce(initialDirection.normalized * initialSpeed, ForceMode.VelocityChange);
			
			StartCoroutine(AutoDestroy());
		}
		
		/// <summary> 
		/// Called when collider2D is trigger and if bullet hits something;
		/// </summary> 
		/// <param name="collider2D">
		/// the collider2D that is hit
		/// </param> 
		void OnTriggerEnter2D(Collider2D collider2D) {
			if ((collider2D.gameObject.tag.Equals("Enemy"))||(collider2D.gameObject.tag.Equals("Player"))) {
				Debug.Log("Colisiono");
				DoDamageTo(collider2D.gameObject);
				DestroyOnHit();
			}else if(collider2D.gameObject.tag.Equals("Wall")) {
				DestroyOnHit();
			}
		}
		
		/// <summary> 
		/// Called when collider2D is not trigger and if bullet hits something;
		/// </summary> 
		/// <param name="collision2D">
		/// the collision2D object with the hit info
		/// </param> 
		void OnCollisionEnter2D(Collision2D collision2D) {
			if ((collision2D.gameObject.tag.Equals(""))||(collision2D.gameObject.tag.Equals("Player"))) {
				DoDamageTo(collision2D.gameObject);
				DestroyOnHit();
			}else if(collider2D.gameObject.tag.Equals("Wall")) {
				DestroyOnHit();
			}
		}
		
		/// <summary> 
		/// Checks if the bullet should be destroyed after hitting something
		/// </summary> 
		void DestroyOnHit() {
			if (destroyOnHit) {
				Destroy(gameObject);
			}
		}
		
		/// <summary> 
		/// Destroy the bullet after "duration" seconds
		/// </summary> 
		IEnumerator AutoDestroy() {
			yield return new WaitForSeconds(lifespan);
			Destroy(gameObject);
		}
	}
}
