﻿using UnityEngine;
using System.Collections;

namespace TheForce {

    /// <remarks> 
    /// Player Controller class
    /// </remarks> 
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(CircleCollider2D))]
    public class PlayerController : Character, IPicker {

        // Main weapon prefab
        public Transform mainWeaponPrefab;
        // The speed of the player walking
        public float walkSpeed = 3;

        // Singleton Instance of PlayerController
        private static PlayerController playerController = null;

        // Property for accesing the Singleton Instance of PlayerController
        public static PlayerController Player{
            get { return playerController; }
        }   

        // Base transform for sprites
        [SerializeField]
        private Transform spriteTransform;
        // Base Transform for weapons
        [SerializeField]
        private Transform aimDirection;

        // main weapon reference
        [SerializeField]
        private Weapon mainWeapon;
        // second weapon reference
        [SerializeField]
        private Weapon secondWeapon;
        // Bomber weapon reference
        [SerializeField]
        private Weapon bomberWeapon;

        // player sprite animator component
        private Animator spriteAnimator;

        /// <summary> 
        /// Initiates the PlayerController checking singleton instance
        /// </summary> 
        protected new void Awake() {
            base.Awake();

            // Check singleton
            if (PlayerController.playerController) {
                Debug.Log("PlayerController: There is already an instance of PlayerController");
                Destroy(gameObject);
            } else {
                PlayerController.playerController = this;
            }

            rigidbody2D.fixedAngle = true;
        }

        /// <summary> 
        /// Initiates the PlayerController checking needs.
        /// </summary> 
        protected void Start() {

            if (!aimDirection) {
                Debug.Log("PlayerController: No AimDirection Transform");
                Destroy(gameObject);
                Application.Quit();
                return;
            }

            if (!spriteTransform) {
                Debug.Log("PlayerController: No Sprite Transform");
                Destroy(gameObject);
                Application.Quit();
                return;
            }

            // Get Sprite Animator        
            spriteAnimator = spriteTransform.GetComponent<Animator>();

            // Checks main weapon
            if (mainWeapon == null) {
                    Debug.Log("PlayerController: No MainWeapon");
                    Destroy(gameObject);
                    Application.Quit();
                    return;
            }

            // Checks secondWeapon weapon

            if (secondWeapon != null) {
                secondWeapon.transform.parent = aimDirection;
                secondWeapon.Owner = this;
            }

            // Checks Bomber weapon

            if (bomberWeapon == null) {
                Debug.Log("PlayerController: No BomberWeapon");
                Destroy(gameObject);
                Application.Quit();
                return;
            }

            bomberWeapon.transform.parent = aimDirection;
            bomberWeapon.Owner = this;

        }

        /// <summary> 
        /// called every frame to update Player state;
        /// </summary> 
        protected new void Update() {
            base.Update();

            // Get Weapon Input and pull/release triggers
            if (Input.GetButtonDown("Fire1")) {
                mainWeapon.PullTrigger();
            }

            if (Input.GetButtonUp("Fire1")) {
                mainWeapon.ReleaseTrigger();
            }

            if (secondWeapon != null) {
                if (Input.GetButtonDown("Fire2")) {
                    secondWeapon.PullTrigger();
                }

                if (Input.GetButtonUp("Fire2")) {
                    secondWeapon.ReleaseTrigger();
                }
            }

            if (bomberWeapon != null) {
                if (Input.GetButtonDown("Bomb")) {
                    bomberWeapon.PullTrigger();
                }

                if (Input.GetButtonUp("Bomb")) {
                    bomberWeapon.ReleaseTrigger();
                }
            }

            // DropWeapon ?
            if (Input.GetButton("DropWeapon")) {
                if (secondWeapon) {
                    DropWeapon();
                }
            }   

            // Aim at mouse position
            Vector3 mousePosition = Input.mousePosition;
            mousePosition.Set(mousePosition.x, mousePosition.y, -Camera.main.transform.position.z);

            Vector3 worldMousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            worldMousePosition.Set(worldMousePosition.x, worldMousePosition.y, transform.position.z);

            Vector3 direction = worldMousePosition - transform.position;
            aimDirection.rotation *= Quaternion.FromToRotation(aimDirection.right, direction);

            // Set Animation state based on angle of rotation and movement input
            // 8 directions so 45º between them.
            float angle     = Vector2.Angle(Vector3.right, aimDirection.right);
            Vector3 cross   = Vector3.Cross(Vector3.right, aimDirection.right);
            
            float verticalInput     = Input.GetAxis("Vertical");
            float horizontalInput   = Input.GetAxis("Horizontal");

            if (verticalInput != 0 || horizontalInput != 0) {
                if (cross.z > 0) {
                    if (angle > 157.5f) {
                        spriteAnimator.SetInteger("Action", 7);
                    } else if (angle >= 112.5f) {
                        spriteAnimator.SetInteger("Action", 8);
                    } else if (angle >= 67.5f) {
                        spriteAnimator.SetInteger("Action", 1);
                    } else if (angle >= 22.5f) {
                        spriteAnimator.SetInteger("Action", 2);
                    } else {
                        spriteAnimator.SetInteger("Action", 3);
                    }
                } else if (cross.z < 0) {
                    if (angle > 157.5f) {
                        spriteAnimator.SetInteger("Action", 7);
                    } else if (angle >= 112.5f) {
                        spriteAnimator.SetInteger("Action", 6);
                    } else if (angle >= 67.5f) {
                        spriteAnimator.SetInteger("Action", 5);
                    } else if (angle >= 22.5f) {
                        spriteAnimator.SetInteger("Action", 4);
                    } else {
                        spriteAnimator.SetInteger("Action", 3);
                        
                    }
                } else { // cross.z == 0
                    if (angle == 0) {
                        spriteAnimator.SetInteger("Action", 3);
                    } else { // angle == 180
                        spriteAnimator.SetInteger("Action", 7);
                    }
                }
            } else { // No input
                spriteAnimator.SetInteger("Action", 0);
            }

        }

        /// <summary> 
        /// called every physics step to move the player;
        /// </summary> 
        void FixedUpdate() {

            // Movement
            float verticalInput     = Input.GetAxis("Vertical");
            float horizontalInput   = Input.GetAxis("Horizontal");

            Vector2 normalizedInput = new Vector2(horizontalInput, verticalInput).normalized;

            Vector2 desiredVelocity = normalizedInput * walkSpeed;
            Vector2 actualVelocity  = rigidbody2D.velocity;
            Vector2 velocityChange  = desiredVelocity - actualVelocity;

            Physics2DExtensions.AddForce(rigidbody2D, velocityChange, ForceMode.VelocityChange);
        }

        /// <summary> 
        /// Method called when colliding with an IPickable object to pick it;
        /// </summary> 
        /// <param name="type">
        /// the type of the pickable object.
        /// </param>
        /// <param name="pickableObject">
        /// The object to pick
        /// </param> 
        public void Pick(PickableType type, object pickableObject) {
            switch (type) {
                case PickableType.weapon:
                    EquipWeapon(((GameObject)pickableObject).GetComponent<Weapon>());
                    break;
                case PickableType.life:
                    Life += (float)pickableObject;
                    break;
            }
        }

        /// <summary> 
        /// Equip second weapon
        /// </summary> 
        /// <param name="weapon">
        /// the weapon to equip
        /// </param>
        public void EquipWeapon(Weapon weapon) {
            secondWeapon = weapon;
            secondWeapon.gameObject.SetActive(true);
            secondWeapon.transform.parent = aimDirection;
            secondWeapon.transform.localScale = Vector3.one;
            secondWeapon.transform.localRotation = Quaternion.identity;
            secondWeapon.transform.localPosition = Vector3.zero;
        }

        /// <summary> 
        /// Drop second weapon
        /// </summary> 
        public void DropWeapon() {
            Destroy(secondWeapon.gameObject);
            secondWeapon = null;
        }
    }
}
