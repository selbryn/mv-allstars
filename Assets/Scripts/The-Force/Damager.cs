﻿using UnityEngine;
using System.Collections.Generic;

namespace TheForce {

    /// <remarks> 
    /// Basic class for objects that deal damage
    /// </remarks> 
    public class Damager : MonoBehaviour, IEnumerable<Damage> {

        // Character owner of the damager if exist
        protected Character owner;

        // Damages list
        [SerializeField] 
        protected List<Damage> damages = new List<Damage>();

        /// <summary> 
        /// Property for accesing the damager owner, the damage dealer
        /// </summary> 
        public Character Owner {
            get { return owner; }
            set { owner = value; }
        }

        /// <summary> 
        /// Property for accesing the list of damages
        /// </summary>
        public List<Damage> Damages {
            get { return damages; }
            set { damages = value; }
        }

        /// <summary> 
        /// Returns the index of the first occurrence of a damage in the damage
        /// damages list. Returns -1 if not found.
        /// </summary> 
        /// <param name="damageType">
        /// The DamageType that we are searching.
        /// </param> -
        protected int indexOf(DamageType damageType) {
            return damages.FindIndex(d => d.type == damageType);
        }

        /// <summary> 
        /// Adds or modify a damage in the damages list
        /// </summary> 
        /// <param name="damageType">
        /// The DamageType that we are adding.
        /// </param>
        /// <param name="resistanceValue">
        /// The damage resitance value.
        /// </param> 
        public void SetDamage(DamageType damageType, float damageValue) {
            int index = indexOf(damageType);
            if (index != -1) {
                damages[index].value = damageValue;
            } else {
                damages.Add(new Damage(damageType, damageValue));
            }
        }

        /// <summary> 
        /// Returns the damage value of a especific damagetype 
        /// Returns 0 if not set.
        /// </summary> 
        /// <param name="damageType">
        /// The DamageType that we are searching.
        /// </param> 
        public float GetDamage(DamageType damageType) {
            int index = indexOf(damageType);
            if(index != -1){
                return damages[index].value;
            }
            return 0;
        }

        /// <summary> 
        /// Removes a damage from the damages list.
        /// resistance. Returns 0 if not set.
        /// </summary> 
        /// <param name="damageType">
        /// The DamageType that is going to be removed.
        /// </param> 
        public void RemoveDamage(DamageType damageType) {
            int index = indexOf(damageType);
            if(index != -1){
                 damages.RemoveAt(index);
            }
        }

        /// <summary> 
        /// Clears the damage list.
        /// </summary> 
        public void ClearDamages() {
            damages.Clear();
        }

        /// <summary> 
        /// Deals Damage to a damageable object
        /// </summary> 
        /// <param name="damageType">
        /// The DamageType that is going to be removed.
        /// </param> 
        protected virtual void DoDamageTo(IDamageable damageable) {
            if (damageable != null) {
                damageable.Damage(this);
            }
        }

        /// <summary> 
        /// Deals Damage to a gameObject object
        /// </summary> 
        /// <param name="damageType">
        /// The DamageType that is going to be removed.
        /// </param> 
        protected virtual void DoDamageTo(GameObject gameObject) {
            IDamageable damageable = (IDamageable)gameObject.GetComponent(typeof(IDamageable));
            if (damageable != null) {
                damageable.Damage(this);
            }
        }

        /// <summary> 
        /// Implementation of IEnumerable<Damage> to be able to iterate through
        /// this object as a damages list
        /// </summary> 
        public IEnumerator<Damage> GetEnumerator() {
            foreach (Damage damage in damages) {
                yield return damage;
            }
        }

        /// <summary> 
        /// Implementation of IEnumerable<Damage> to be able to iterate through
        /// this object as a damages list
        /// </summary> 
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
    }
}
