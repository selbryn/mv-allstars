﻿using UnityEngine;
using System.Collections;

namespace TheForce {

    /// <remarks> 
    /// Interface for objects that need a trigger to start an action
    /// like weapons.
    /// </remarks> 
    public interface ITriggerable {

        void PullTrigger();

        void ReleaseTrigger();

        void Trigger();

    }

    /// <summary> 
    /// Interface for objects that can be damaged by others.
    /// </summary> 
    public interface IDamageable {

        void Damage(Damager damager);

    }

    /// <summary> 
    /// Interface for objects that can be killed instantly by others
    /// </summary> 
    public interface IKillable {

        void Kill();

    }

    /// <summary> 
    /// Interface for objects that can pick pickable items
    /// </summary> 
    public interface IPicker {
        void Pick(PickableType type, object pickableObject);
    }
}