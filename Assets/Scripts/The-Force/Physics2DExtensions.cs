﻿using UnityEngine;

namespace TheForce {

    /// <remarks> 
    /// Extension of the 2D physics in unity.
    /// </remarks> -------------------------------------------------------
    public static class Physics2DExtensions {

        /// <summary> 
        /// Replicates de behaviour of rigidbody.addforce with forcemode in 2D
        /// </summary> 
        /// <param name="rigidbody2D">
        /// The rigidbody on wich the force will be added;
        /// </param> 
        /// <param name="force">
        /// The force that will be added to the rigibody;
        /// </param>
        /// <param name="mode">
        /// The ForceMode that will be used;
        /// </param> --
        public static void AddForce(this Rigidbody2D rigidbody2D, Vector2 force, ForceMode mode = ForceMode.Force) {
            switch (mode) {
                case ForceMode.Force:
                    rigidbody2D.AddForce(force);
                    break;
                case ForceMode.Impulse:
                    rigidbody2D.AddForce(force / Time.fixedDeltaTime);
                    break;
                case ForceMode.Acceleration:
                    rigidbody2D.AddForce(force * rigidbody2D.mass);
                    break;
                case ForceMode.VelocityChange:
                    rigidbody2D.AddForce(force * rigidbody2D.mass / Time.fixedDeltaTime);
                    break;
            }
        }
    }
}