﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CircleCollider2D))]
public class EnemySensor : MonoBehaviour {

	private GameObject target = null;
	private Juancho.EnemyBase enemyBase;

	// Use this for initialization
	void Start () {
		enemyBase = transform.parent.gameObject.GetComponent<Juancho.EnemyBase> ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter2D(Collider2D collision) {			
		if (collision.gameObject.tag.Equals("Player")) {
			Debug.Log("Found Player");
			enemyBase.TargetFound = true;
			enemyBase.HasHit = false;
			enemyBase.Target = collision.gameObject.transform;
			if (enemyBase.ReturnPosition == Vector2.zero)
				enemyBase.ReturnPosition = transform.position;
			enemyBase.OutOfSensor = false;
		}
	}

	void OnTriggerExit2D(Collider2D collision) {
		if (collision.gameObject.tag.Equals("Player")) {
			Debug.Log("Player is Out of Sensor..");
			enemyBase.OutOfSensor = true;
		}
	}
}
