﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {

	Juancho.StateMachine stateMachine;

	void Awake() {
	}
	// Use this for initialization
	void Start () {
		stateMachine = GetComponent<Juancho.StateMachine> ();
	}
	
	// Update is called once per frame
	void Update () {
		float x = Input.GetAxis ("Horizontal");
		float y = Input.GetAxis ("Vertical");
		if (x > 0) {
			if (y > 0)
				stateMachine.state = Juancho.State.MovingRightUp;
			else if (y < 0)
				stateMachine.state = Juancho.State.MovingRightDown;
			else
				stateMachine.state = Juancho.State.MovingRight;
		} else if (x < 0) {
			if (y > 0)
				stateMachine.state = Juancho.State.MovingLeftUp;
			else if (y < 0) 
				stateMachine.state = Juancho.State.MovingLeftDown;
			else 
				stateMachine.state = Juancho.State.MovingLeft;
		} else {
			if (y > 0) 
				stateMachine.state = Juancho.State.MovingUp;
			else if (y < 0)
				stateMachine.state = Juancho.State.MovingDown;
			else
				stateMachine.state = Juancho.State.Idle;
		}
		if (Input.GetKey(KeyCode.Space))
		    stateMachine.isAttacking(true);
		if (Input.GetKeyUp (KeyCode.Space))
			stateMachine.isAttacking (false);

		Debug.Log (x + " " + y);
		Debug.Log (stateMachine.state.ToString());

		rigidbody2D.AddForce (new Vector3 (10*x, 10*y));
	}
}
