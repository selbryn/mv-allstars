﻿using UnityEngine;
using System.Collections;

namespace Juancho {
	
	[RequireComponent(typeof(Rigidbody2D))]
	public class EnemyBase : TheForce.Character {
		
		public enum MoveType {
			Hold, LinealPatrol, RoundPatrol
		};
		
		public enum AttackType {
			Bomb, Ranged, Melee, Turret
		}

		public enum StartingRotation {
			Right, Left, Up, Down
		}

		public enum Rotation {
			Right, Left
		}
		
		[SerializeField]
		private AttackType attackType = AttackType.Bomb;

		[SerializeField]
		private TheForce.Bullet bullet;

		[SerializeField]
		private MoveType moveType = MoveType.RoundPatrol;

		[SerializeField]
		private StartingRotation startingRotation;

		[SerializeField]
		private Rotation rotation;
		
		private Juancho.StateMachine stateMachine;

		//Movement related vars
		[SerializeField]
		private float damping = 1F;
		[SerializeField]
		private float speed = 1F;

		private Vector3 direction;
		private Vector3 distance;
		private Vector3 nextDirection;
		private Vector3 respawn;

		//Fatigue system vars
		[SerializeField]
		private bool EnableFatigue = false;
		[SerializeField]
		private float fatigue = 1f;
		private float deltaTime = 0f;
		private Vector2 returnPosition = Vector2.zero;
		private bool isReturning = false;

		//Attack related vars
		private bool targetFound = false;
		private Transform target = null;
		private bool outOfSensor = false;

		private TheForce.Damager damager;
		
		[SerializeField]
		private float damage = 300;
		[SerializeField]
		private float attackSpeed = 0.2f;
		private float attackDelta = 0;
		private RaycastHit2D hit;
		private bool hasHit = false;
		
		public virtual bool TargetFound{
			set{ targetFound = value; }
		}
		
		public virtual Transform Target{
			set{ target = value; }
		}

		public virtual bool HasHit{
			set{ hasHit = value; }
			get{ return hasHit; }
		}

		public virtual bool OutOfSensor{
			set{ outOfSensor = value; }
		}

		public Vector2 ReturnPosition{
			set{ returnPosition = value; }
			get{ return returnPosition; }
		}
		
		void Awake() {
			base.Awake ();
		}
		
		void Start () {
			initConfig ();
		}

		/// <summary>
		/// Initial config.
		/// </summary>
		private void initConfig() {
			respawn = transform.position;

			//sets the initial rotation position from inspector.
			if (startingRotation.Equals(StartingRotation.Right)) {
				direction = transform.right;
			}else if (startingRotation.Equals(StartingRotation.Left)) {
				direction = -transform.right;
			}else if (startingRotation.Equals(StartingRotation.Up)) {
				direction = transform.up;
			}else if (startingRotation.Equals(StartingRotation.Down)) {
				direction = -transform.up;
			}
			stateMachine = GetComponent<Juancho.StateMachine> ();
			stateMachine.state = Juancho.State.Idle;
			
			damager = gameObject.AddComponent("Damager") as TheForce.Damager;
			damager.Owner = this;
			damager.SetDamage (TheForce.DamageType.Basico, damage);

			attackDelta = attackSpeed;

			if (moveType.Equals(MoveType.LinealPatrol)) {
				nextDirection = -direction;
			}else if (moveType == MoveType.RoundPatrol) {
				nextDirection = getNextRotation();
			}
		}
		
		void Update () {
			base.Update ();
			iaHandler ();
			stateMachineHandler ();
		}

		/// <summary>
		/// Handles the Enemy IA.
		/// </summary>
		private void iaHandler() {
			if (!targetFound) {
				movement();
			}else{
				attack();
			}
			if (isReturning)
				returnToPosition();
		}

		/// <summary>
		/// Movement logic.
		/// </summary>
		private void movement() {
			if ((!attackType.Equals(AttackType.Turret))&&(!moveType.Equals(MoveType.Hold))) {
				if (hasHit) {
					Debug.DrawLine (transform.position, hit.point, Color.green);
					rigidbody2D.velocity = (distance * damping).normalized * speed;
					float distanceToWall = (hit.point - new Vector2(transform.position.x, transform.position.y)).magnitude;
					if (distanceToWall <= gameObject.transform.renderer.bounds.size.x) {
						direction = nextDirection;
						nextDirection = getNextRotation();
						hasHit = false;
					}
				}else{
					Vector3 fwd = transform.TransformDirection(direction);

					hit = Physics2D.Raycast (transform.position, fwd,
					                         Mathf.Infinity, -LayerMask.NameToLayer ("Wall"));
					if (hit != null) {
						hasHit = true;
						distance = (hit.point - new Vector2(transform.position.x, transform.position.y));
					}
				}
			}
			attackDelta = 0;
		}

		/// <summary>
		/// Attack logic
		/// </summary>
		private void attack() {
			switch(attackType) {
				case AttackType.Bomb: bombAttack(); break;
				case AttackType.Turret: turretAttack(); break;
				case AttackType.Ranged: rangedAttack(); break;
			}
		}

		/// <summary>
		/// Fatigue System, if fatigue time consumes, returns to its previous position.
		/// </summary>
		private void returnToPosition() {
			float position = TruncateFloat (((Vector2)transform.position).magnitude, 2);
			float returnPos = TruncateFloat (returnPosition.magnitude, 2);

			if ( ((returnPos - 0.1f) < position)&&(position < (returnPos + 0.1f)) ) {
				isReturning = false;
				hasHit = false;
				targetFound = false;
				target = null;
				deltaTime = 0;
				Debug.Log("Patrolling again...");
			}else{
				Vector2 velocity = (returnPosition - new Vector2(transform.position.x, transform.position.y));
				rigidbody2D.velocity = (velocity * damping).normalized * speed;

			}
		}

		/// <summary>
		/// Suicide attack. (Goes to the target and explodes).
		/// </summary>
		private void bombAttack() {
			Vector2 fwd = target.position - transform.position;

			hit = Physics2D.Raycast (transform.position, fwd,
			                         Mathf.Infinity, -LayerMask.NameToLayer ("Player"));
			if (hasHit) {
				Debug.DrawLine (transform.position, hit.point, Color.red);
				Vector2 distance = (hit.point - new Vector2(transform.position.x, transform.position.y));
				rigidbody2D.velocity = (distance * damping).normalized * speed;

				if (EnableFatigue) {
					deltaTime += Time.deltaTime;
					if (deltaTime >= fatigue) {
						isReturning = true;
					}
				}else{
					isReturning = false;
					deltaTime = 0;
				}
			}else{
				if (hit != null) {
					hasHit = true;
				}
			}
		}

		/// <summary>
		/// Turret Mode. (stays in place and attacks player on sight)
		/// </summary>
		private void turretAttack() {
			Vector2 fwd = target.position - transform.position;
			
			hit = Physics2D.Raycast (transform.position, fwd,
			                         Mathf.Infinity, -LayerMask.NameToLayer ("Player"));
			if (hasHit) {
				Debug.DrawLine (transform.position, hit.point, Color.red);
				//if the targets goes out of sight, just leaves attack mode.
				if (outOfSensor) {
					targetFound = false;
					target = null;
				}else{
					if (attackDelta >= attackSpeed) {
						//TODO arreglar el position del Instantiate.
						Vector2 position = transform.position;

						//esto es copy+paste de The-Force, no se muy bien porque autoincrementa el daño basico y eso.
						//pero lo he dejado igual.
						TheForce.Bullet bulletTemp = (TheForce.Bullet) Instantiate(bullet, position, Quaternion.identity);
						float basicDamage = bulletTemp.GetDamage(TheForce.DamageType.Basico);
						basicDamage +=  basicDamage;
						bulletTemp.SetDamage(TheForce.DamageType.Basico, basicDamage);
						bulletTemp.Owner = this;
						bulletTemp.initialDirection = fwd;
						bulletTemp.initialSpeed += bulletTemp.initialSpeed;

						attackDelta = 0;
					}else{
						attackDelta += Time.deltaTime;
					}
				}
			}else{
				if (hit != null) {
					hasHit = true;
				}
			}
		}

		/// <summary>
		/// Ranged Attack
		/// </summary>
		private void rangedAttack() {
			Vector2 fwd = target.position - transform.position;
			
			hit = Physics2D.Raycast (transform.position, fwd,
			                         Mathf.Infinity, -LayerMask.NameToLayer ("Player"));
			if (hasHit) {
				Debug.DrawLine (transform.position, hit.point, Color.red);

				//if the target goes out of sensor, we move to its position and try to find it.
				if (outOfSensor) {
					Debug.DrawLine (transform.position, hit.point, Color.red);
					Vector2 distance = (hit.point - new Vector2(transform.position.x, transform.position.y));
					rigidbody2D.velocity = (distance * damping).normalized * speed;
					
					if (EnableFatigue) {
						deltaTime += Time.deltaTime;
						if (deltaTime >= fatigue) {
							isReturning = true;
						}
					}else{
						isReturning = false;
						deltaTime = 0;
					}
				}else{
					rigidbody2D.velocity = Vector2.zero;

					if (attackDelta >= attackSpeed) {

						//TODO arreglar la posicion del Instantiate de la bala
						Vector2 position = transform.position;
						
						TheForce.Bullet bulletTemp = (TheForce.Bullet) Instantiate(bullet, position, Quaternion.identity);
						float basicDamage = bulletTemp.GetDamage(TheForce.DamageType.Basico);
						basicDamage +=  basicDamage;
						bulletTemp.SetDamage(TheForce.DamageType.Basico, basicDamage);
						bulletTemp.Owner = this;
						bulletTemp.initialDirection = fwd;
						bulletTemp.initialSpeed += bulletTemp.initialSpeed;
						
						attackDelta = 0;
					}else{
						attackDelta += Time.deltaTime;
					}
				}
			}else{
				if (hit != null) {
					hasHit = true;
				}
			}

		}

		/// <summary>
		/// Gets the next rotation in the patrol logic
		/// </summary>
		/// <returns>The next rotation.</returns>
		private Vector3 getNextRotation() {
			if (moveType.Equals(MoveType.RoundPatrol)) {
				if (rotation.Equals(Rotation.Right)) {
					if (direction.Equals(transform.right))
						return -transform.up;
					else if (direction.Equals(-transform.up))
						return -transform.right;
					else if (direction.Equals(-transform.right))
						return transform.up;
					else if (direction.Equals(transform.up))
						return transform.right;
				}else if (rotation.Equals(Rotation.Left)) {
					if (direction.Equals(transform.right))
						return transform.up;
					else if (direction.Equals(transform.up))
						return -transform.right;
					else if (direction.Equals(-transform.right))
						return -transform.up;
					else if (direction.Equals(-transform.up))
						return transform.right;
				}
			}else if(moveType.Equals(MoveType.LinealPatrol)) {
				return -direction;
			}
			return Vector3.zero;
		}

		/// <summary>
		/// State Machine Handler
		/// </summary>
		private void stateMachineHandler() {
			int x = (int) rigidbody2D.velocity.x;
			int y = (int) rigidbody2D.velocity.y;
			
			if (x > 0) {
				if (y > 0)
					stateMachine.state = Juancho.State.MovingRightUp;
				else if (y < 0)
					stateMachine.state = Juancho.State.MovingRightDown;
				else
					stateMachine.state = Juancho.State.MovingRight;
			} else if (x < 0) {
				if (y > 0)
					stateMachine.state = Juancho.State.MovingLeftUp;
				else if (y < 0) 
					stateMachine.state = Juancho.State.MovingLeftDown;
				else 
					stateMachine.state = Juancho.State.MovingLeft;
			} else {
				if (y > 0) 
					stateMachine.state = Juancho.State.MovingUp;
				else if (y < 0)
					stateMachine.state = Juancho.State.MovingDown;
				else
					stateMachine.state = Juancho.State.Idle;
			}
		}

		/// <summary>
		/// Truncates a float for better positioning.
		/// </summary>
		/// <returns>The float.</returns>
		/// <param name="number">Number to truncate</param>
		/// <param name="decimals">Decimals to truncate</param>
		float TruncateFloat(float number, int decimals) {
			float solution = 0;

			for(int i = 0; i < decimals; i++) {
				number *= 10;
			}
			int aux = (int) number;
			solution = (float) aux;

			for(int i = 0; i < decimals; i++) {
				solution /= 10;
			}
			return solution;
		}

		void OnCollisionEnter2D(Collision2D collision) {
			if (collision.gameObject.tag.Equals ("Enemy")) {
				isReturning = true;
			}
			if (collision.gameObject.tag.Equals("Player")) {
				Debug.Log("PUUUM");
				collision.gameObject.GetComponentInChildren<TheForce.Character>().Damage(damager);
				Kill ();
			}
		}
		
		
	}
	
}
