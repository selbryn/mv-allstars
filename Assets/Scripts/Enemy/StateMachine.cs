﻿using UnityEngine;
using System.Collections;

namespace Juancho {

	//List of possible states (can be accessed from StateMachie.State)
	public enum State{
		Idle, MovingLeft, MovingRight, MovingUp, MovingDown, MovingLeftUp, MovingRightUp,
		MovingLeftDown, MovingRightDown, UsingItem, Dying, Respawning
	}

	[RequireComponent(typeof(Animator))]
	/// <summary>
	/// State machine for Enemies.
	/// </summary>
	public class StateMachine : MonoBehaviour {

		private State currentState = State.Idle;

		private Animator animator;

		private bool attackState = false;
		private bool item = false;

		public virtual State state{
			get { return currentState; }
			set { currentState = value; }
		}

		/// <summary>
		/// Prepares the attacking State
		/// </summary>
		/// <param name="attack">If set to <c>true</c> attack.</param>
		public void isAttacking(bool attack) {
			this.attackState = attack;
		}

		/// <summary>
		/// Sets the dead state.
		/// </summary>
		private void kill() {
			animator.SetBool("dead", true);
		}

		/// <summary>
		/// Sets the animator variables.
		/// </summary>
		/// <param name="x">Sets the X param</param>
		/// <param name="y">Sets the Y param</param>
		/// <param name="item">If set to <c>true</c> uses an item</param>
		private void setAnimatorVars(int x, int y, bool item) {
			animator.SetInteger ("x", x);
			animator.SetInteger ("y", y);
			animator.SetBool ("item", item);
		}

		public void Start() {
			animator = GetComponent<Animator> ();
			state = State.Idle;
		}

		public void Update() {
			switch (currentState) {
			case State.Idle: setAnimatorVars(0,0,false);break;
			case State.MovingRight: setAnimatorVars(1,0,false);break;
			case State.MovingLeft: setAnimatorVars(-1,0,false);break;
			case State.MovingUp: setAnimatorVars(0,1,false);break;
			case State.MovingDown: setAnimatorVars(0,-1,false);break;
			case State.MovingRightUp: setAnimatorVars(1,1,false);break;
			case State.MovingRightDown: setAnimatorVars(1,-1,false);break;
			case State.MovingLeftUp: setAnimatorVars(-1,1,false);break;
			case State.MovingLeftDown: setAnimatorVars(-1,-1,false);break;
			case State.UsingItem: break;
			case State.Dying: kill(); break;
			}

			animator.SetBool ("attack", attackState);
		}
	}
}
