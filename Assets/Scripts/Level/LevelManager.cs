using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
namespace Scenario {
	public enum Directions{North,South,West,East};
	public enum Levels{
		Tipo1,Tipo2};
	public enum Rooms{
		Initial,Easy,Medium,Hard,Shop,Boss,Weapon};
	public enum Doors{
		Normal,Enemies,Key,Switch,Hidden,Weapon,Boss,Shop}

public class LevelManager : MonoBehaviour {

	public Levels levelType;

	public Vector3 initialPosition;

	public int roomHeight, roomWidth;

	public int nRooms; // Numero de habitaciones que tendra el nivel completo


	public float shopChance;
	public float bossChance;
	public float weaponChance;
	public float weaponVisibleChance;
	
	public float distanceWeight; // Importancia de la distancia para calcular la posibilidad de una nueva sala
	public float neighbourWeight; // Importancia de las salas vecinas para calcular la posibilidad de una nueva sala
	
	public float hardChance;
	public float mediumChance;
		
	private	int totalRooms=0; //Numero de habitaciones que hay en el momento
	
	private Queue roomsQueue; // Cola de las habitaciones pendientes de "conectar" con otras
	
	private List<GameObject> levelRooms; // Lista de todas las habitaciones del nivel
	
	private float vOffset,hOffset; //Dstancia vertical/horizontal entre las habitaciones

		public Transform playerPrefab; //Referencia al prefab del player
	
	public bool hasShop = false;
	public bool hasBoss = false;
	public bool hasWeapon = false;


		

	// Use this for initialization
	void Start () {

		roomsQueue = new Queue();
		generateLevel ();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void generateLevel()
	{
		string levelPath;
		Vector3 roomPosition = Vector3.zero;
		RoomManager rm1; // Script encargado de generar las salas
		totalRooms = 0; // Numero de habitaciones instanciadas en el nivel
		GameObject room1;
		levelRooms=new List<GameObject>();
		

		//Creamos la sala de inicio y la metemos en el stack
		levelPath = getSingleRoomPath (levelType, Rooms.Initial);
		room1 = Instantiate (Resources.Load (levelPath),initialPosition,Quaternion.identity) as GameObject;
		roomsQueue.Enqueue (room1);
		room1.name = totalRooms + " - Initial";

		//Actualizamos la distancia entre salas
		rm1 = room1.GetComponent<RoomManager>();
			rm1.RoomType = Rooms.Initial;
		vOffset = rm1.blockSize*roomHeight; 
		hOffset = rm1.blockSize*roomWidth; 

		//Añadimos la sala a la lista de todas las salas del nivel
		levelRooms.Add (room1);
		totalRooms++;
		

		GameObject player = Instantiate (playerPrefab, initialPosition, Quaternion.identity) as GameObject;
	
		
		while (totalRooms<nRooms) 
		{
			//Si la cola esta vacia (porque el selector no cumpla la probabilidad o no haya salidas disponibles), 
			//volvemos a llenarla empezando desde el final, para cumplir el total de salas	
			if(roomsQueue.Count<1)
			{
					restartRoomQueue();
			}
			// Obtenemos del stack la habitacion que vamos a conectar con las nuevas
			
			room1 = (GameObject)roomsQueue.Dequeue ();
			rm1 = room1.GetComponent<RoomManager>();

			//Generamos un numero aleatorio y una probabilidad aleatoria de que se genere una nueva sala
			//La probabilidad esta basada en la distancia al inicio, y en el numero de salas vecinas que existen ya
			
			int nExists= rm1.getExitCount();
			int selector = Random.Range (0,100);
			float roomChance=100 - (rm1.Distance*distanceWeight + (4-nExists)*neighbourWeight);
			
			if(selector<=roomChance)
			{
				//Obtenemos una direccion aleatoria desde la sala en la que estamos y comprobamos que no exista una sala en la posicion
				bool validExit=false;
				Directions direction=Directions.North;
				
				while(rm1.getExitCount ()>0 && !validExit)
				{
					direction=rm1.getRandomExit();
					validExit=!isRoomAtDirection (rm1.transform.position,direction);
					//Si en esa direccion ya hay una sala, eliminamos la salida de de esa sala
					if(!validExit)
					{
						rm1.removeExit (direction);
					}
				
				}

				//Si encontramos una salida correcta generamos una nueva sala
				if(validExit)
				{
					generateRoom (room1,direction);
					totalRooms++;
					roomsQueue.Enqueue (room1);
				}
			}

		}

		levelRooms [0].GetComponent<RoomManager> ().enableDoors ();
		
	}
	
	//Funcion que devuelve la ruta de una sala cuando solo tenemos una de su tipo ( Boss, Tienda )
	private string getSingleRoomPath(Levels levelType,Rooms roomType)
	{
		string path=("Levels/" + levelType +"/"+ roomType+"/"+roomType);

		return path;
	}
	//Funcion que nos devuelve la ruta de una habitacion aleatoria 
	//dentro de la carpeta correspondiente al TipoNivel/TipoHabitacion(Facil, dificil, boss... )

	private string getRandomRoomPath(Levels levelType,Rooms roomType)
	{
		//Creamos la ruta absoluta correspondiente al tipo de nivel y habitacion
		string path = Application.dataPath + "/Resources/Levels/" + levelType + "/" + roomType+"/";
		// Se obtiene el total de archivos en la carpeta, y se divide entre 2 por los .meta de los prefabs 
		int nFiles = Directory.GetFiles (path).Length / 2; 
		//Seleccionamos un numero aleatorio, que sera el numero de la habitacion que generaremos
		int roomNo = Random.Range (0, nFiles);

		//Creamos el path relativo a la carpeta Resouces para poder hacer el Resources.Load
		path = "Levels/" + levelType + "/" + roomType+"/"+roomType+ roomNo;
		return path;
	}

	//Funcion encagada de la generacion de nuevas salas
	private void generateRoom(GameObject room1, Directions dir)
	{
		RoomManager rm1 = room1.GetComponent <RoomManager>();
		RoomManager rm2=null;
		string levelPath;
		Vector3 pos;
		GameObject room2=null;
		Rooms roomType = Rooms.Easy;
		
		//Si estamos colocando la ultima sala y aun no tenemos sala de boss, forzamos a que esta sea Boss
		//En caso contrario, generaremos una aleatoria
		if(!hasBoss && totalRooms==nRooms-1)
		{
			roomType=Rooms.Boss;
		}
		else
		{
			roomType = getRandomRoomType (rm1.Distance);
			
		}
		
		//Si el tipo de sala es Boss o Tienda( En principio solo hay un modelo) obtenemos su path
		//De los otros tipos, obtenemos un modelo aleatorio
		if(roomType.Equals(Rooms.Shop) || roomType.Equals(Rooms.Boss) || roomType.Equals(Rooms.Weapon))
		{
			levelPath=getSingleRoomPath (levelType,roomType);
		}
		else
		{
			levelPath = getRandomRoomPath (levelType, roomType);
		}
		

		//Generamos la sala en la correspondiente direccion, y conectamos la sala origen con la nueva
		switch (dir) {

		case Directions.North:

			//Actualizamos la posicion de la nueva sala en funcion de la anterior y el offSet
			pos = room1.transform.position;	
			pos.y += vOffset;
			
			//Instanciamos la sala y las conectamos
			room2 = Instantiate(Resources.Load(levelPath),pos,Quaternion.identity) as GameObject ;			
			rm2 = room2.GetComponent <RoomManager>();

			rm1.connectRoom (room2,Directions.North,rm2.RoomType);
			rm2.connectRoom (room1,Directions.South,rm1.roomType);

			break;

		case Directions.South:
			
			//Actualizamos la posicion de la nueva sala en funcion de la anterior y el offSet
			pos = room1.transform.position;
			pos.y -= vOffset;
			
			//Instanciamos la sala y las conectamos
			room2 = Instantiate(Resources.Load(levelPath),pos,Quaternion.identity) as GameObject ;

			rm2 = room2.GetComponent <RoomManager>();
			
				rm1.connectRoom (room2,Directions.South,rm2.RoomType);
				rm2.connectRoom (room1,Directions.North,rm1.RoomType);

			break;

		case Directions.East:
			pos = room1.transform.position;
			
			pos.x += hOffset;
			
			room2 = Instantiate(Resources.Load(levelPath),pos,Quaternion.identity) as GameObject ;

			rm2 = room2.GetComponent <RoomManager>();
			
				rm1.connectRoom (room2,Directions.East,rm2.RoomType);
				rm2.connectRoom (room1,Directions.West,rm1.RoomType);

			break;

		case Directions.West:
			pos = room1.transform.position;
			
			pos.x-= hOffset;
			
			
			room2 = Instantiate(Resources.Load(levelPath),pos,Quaternion.identity) as GameObject ;
			rm2 = room2.GetComponent <RoomManager>();
			
				rm1.connectRoom (room2,Directions.West,rm2.RoomType);
				rm2.connectRoom (room1,Directions.East,rm1.RoomType);

			break;
		default:
			break;
		};
		

		//Actualizamos la distancia de la nueva sala
		rm2.Distance=rm1.Distance+1;
		
		//Actualizamos el tipo de sala
			rm2.RoomType = roomType;

			//Desactivamos los enemigos hasta que el player entre en la sala
			rm2.disableEnemies ();

			room2.name=totalRooms+" - "+roomType+" ("+rm2.Distance+")";
		levelRooms.Add (room2);

		//Si la sala es de tipo boss o arma (oculta), no la añadiremos a la cola para añadirle vecinas
		if (!roomType.Equals (Rooms.Boss) && !roomType.Equals (Rooms.Weapon))
		{
			roomsQueue.Enqueue (room2);
		}

		


		

	}


		//Funcion que nos devuelve un tipo de sala en funcion de la distancia a la que nos encontramos de la sala de inicio
		//Si no hay sala de tienda o boss en el nivel, habra una posibilidad(que ira incrementando ) de que la sala sea de esos tipos
		//La posibilidad para cada dificultad es:
		//DIFICIL: Distancia*0.1, MEDIO: Distancia*0.2 , Facil: El resto
		public Rooms getRandomRoomType(int Distance)
		{
			Rooms roomType=Rooms.Easy;
			float selector = Random.Range (0f, 100f);

			//Si no hay tienda en el nivel, generamos una con una probabilidad que ira incrementando
			if (!hasShop) 
			{
				if(selector<shopChance)
				{
					roomType=Rooms.Shop;
					hasShop=true;
				}
				shopChance+=10f;
			}
			//Si no hay sala de arma en el nivel, generamos una con una probabilidad que ira incrementando
			else if (!hasWeapon) 
			{
				if(selector<weaponChance)
				{
					roomType=Rooms.Weapon;
					hasWeapon=true;
				}
				shopChance+=10f;
			}
			//Si no hay boss en el nivel, generamos una con una probabilidad que ira incrementando
			else if(!hasBoss)
			{
				if(selector<bossChance)
				{
					roomType=Rooms.Boss;
					hasBoss=true;
				}
				bossChance+=5f;
			}

			if(roomType.Equals (Rooms.Easy))
			 {
			//Seleccionamos una de las 3 dificultades con una probabilidad dependiente de la distancia
				if(selector<hardChance*Distance)
				{
					roomType=Rooms.Hard;
				}
				else if(selector<mediumChance*Distance)
				{
					roomType=Rooms.Medium;
				}
				else
				{
					roomType=Rooms.Easy;
				}
			}
			return roomType;
		}
	
	//Funcion que comprueba si una sala ya existe para evitar repeticiones
	private bool roomAlreadyExists(string roomName)
	{
		bool found=false;
		
			foreach (GameObject room in levelRooms) 
			{
				if(room.name.Contains (roomName))
				{
					found=true;
					break;
				}
			}

		return found;
	}
		//Funcion que comprueba si una sala ya existe en una posicion
	public bool isRoomAtPosition(Vector3 pos)
	{
		bool found = false;
	
		foreach (GameObject room in levelRooms) 
		{
			if(room.transform.position.Equals (pos))
			{
				found=true;
				break;
			}
		}

		return found;
	}
	
	//Funcion que comprueba si existe una sala en una direccion
	public bool isRoomAtDirection(Vector3 pos, Directions dir)
	{
		Vector3 newPosition=pos;
		switch(dir)
		{
			case Directions.North:
				newPosition.y+=vOffset;
				break;
			case Directions.South:
				newPosition.y-=vOffset;
				break;
			case Directions.East:
				newPosition.x+=hOffset;
				break;
			case Directions.West:
				newPosition.x-=hOffset;
				break;
			default:
			break;
		};
			return isRoomAtPosition (newPosition);
	}
	
	//Funcion que se utiliza en caso de que se vacie la cola para conectar las salas, y que vuelve a rellenar la cola empezando desde el final
	private void restartRoomQueue()
	{
			for (int i=levelRooms.Count-1; i>=0; i--)
			{
				GameObject room = levelRooms[i];
				if(!room.name.Contains ("Boss"))
				{
					roomsQueue.Enqueue (room);
				}
			}
	}

	//Funcion para mover la camara a la habitacion destino
	private void moveCamera(GameObject roomDest)
	{
			Vector3 pos = roomDest.transform.position;
			pos.y -= (vOffset / 2) -1f;
			pos.x += (hOffset / 2) -1f;
			pos.z = -100;
			Camera.main.transform.Translate (pos);
	}
}
}
