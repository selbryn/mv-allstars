﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using TheForce;
using Juancho;

namespace Scenario
{
		public class RoomManager : MonoBehaviour
		{
	
				public int roomHeight, roomWidth; // Tamaño ( largo, ancho) del room

				public Vector3 initialPosition; // Posicion inicial donde se empieza a generar la sala
	
				public Vector3 playerPosition; // Posicion del jugador al cargar la sala
	
				public Rooms roomType;  //Tipo de sala
				
				public Rooms RoomType {
						get {
								return roomType;
						}
						set {
								roomType = value;
						}
				}

				private int distance = 0;


				//Distancia ( en numero de salas ) a la habitacion de inicio
				public int Distance {
						get {
								return this.distance;
						}
						set {
								distance = value;
						}
				}
	
				public float blockSize; // Tamaño de los bloques que se usan en la sala

				private GameObject roomN, roomW, roomS, roomE; // Salas adyacentes en el correspondiente punto cardinal

				public GameObject doorN, doorW, doorS, doorE; // Puertas del correspondiente punto cardinal, deben ser asignadas desde el entorno

				public List<Directions> freeExists; // Salidas disponivles en la sala

				public List<GameObject> doors;
				public List<GameObject> enemyList;

				void Awake ()
				{
						doors = new List<GameObject> ();
						freeExists = new List<Directions> ();

						freeExists.Add (Directions.North);
						freeExists.Add (Directions.South);
						freeExists.Add (Directions.West);
						freeExists.Add (Directions.East);
						
						//Actualizamos los enemigos con la referencia a la sala
						/*
						foreach (GameObject enemy in enemyList) 
						{
				enemy.GetComponent<EnemyBase>().Room=this.transform.root;
						}

			*/
				}

				// Use this for initialization
				void Start ()
				{

				}
	
				// Update is called once per frame
				void Update ()
				{
		
				}

				public int getExitCount ()
				{
						return freeExists.Count;
				}
	
				public Directions getRandomExit ()
				{
						Directions exit;

						int index = Random.Range (0, freeExists.Count);
						exit = freeExists [index];

						return exit;

				}

				//Conecta la sala con otra. Dependiendo del tipo de la nueva sala, la puerta sera diferente 
				// para indicarnos que tipo de sala nos vamos a encontrar
				public void connectRoom (GameObject room, Directions direction, Rooms roomType)
				{
						Doors doorType = Doors.Normal;

						switch (roomType) {
						case Rooms.Boss:
								doorType = Doors.Boss;
								break;
						case Rooms.Shop:
								doorType = Doors.Shop;
								break;
						case Rooms.Weapon:
								int selector = Random.Range (0, 100);

				
								if (selector <= 50) {
										doorType = Doors.Hidden;
								} else {
										doorType = Doors.Weapon;
								}
								break;
						default:
								if (enemyList.Count > 0) {
										doorType = Doors.Enemies;
								} else {
										doorType = Doors.Normal;
								}
								break;

						}
						;
						switch (direction) {
						case Directions.North:
								doorN.GetComponent <DoorManager> ().setDoor (doorType);
								doorN.GetComponent <DoorManager> ().Room = this.transform.root;
								doorN.GetComponent <DoorManager> ().Direction = direction;
								doors.Add (doorN);
								roomN = room;
								break;
						case Directions.South:
								doorS.GetComponent <DoorManager> ().setDoor (doorType);
								doorS.GetComponent <DoorManager> ().Room = this.transform.root;
								doorS.GetComponent <DoorManager> ().Direction = direction;
								doors.Add (doorS);
								roomS = room;
								break;
						case Directions.East:
								doorE.GetComponent <DoorManager> ().setDoor (doorType);
								doorE.GetComponent <DoorManager> ().Room = this.transform.root;
								doorE.GetComponent <DoorManager> ().Direction = direction;
								doors.Add (doorE);
								roomE = room;
								break;
						case Directions.West:
								doorW.GetComponent <DoorManager> ().setDoor (doorType);
								doorW.GetComponent <DoorManager> ().Room = this.transform.root;
								doorW.GetComponent <DoorManager> ().Direction = direction;
								doors.Add (doorW);
								roomW = room;
								break;
						default:
								break;
						}
						;
						freeExists.Remove (direction);
				}

				
				//Movemos la camara de forma fluida hacia la siguiente sala
				public IEnumerator moveCamera (Vector3 target, float duration)
				{

						float t = 0.0f;
						Vector3 startingPos = Camera.main.transform.position;
						while (t < 1.0f && !startingPos.Equals(target)) {
								t += Time.deltaTime * (Time.timeScale / duration);
			
								Camera.main.transform.position = Vector3.Lerp (startingPos, target, t);
								yield return 0;
			
						}

				}
				
				//Mueve el player para colocarlo en la puerta correspondiente de la nueva sala
				public IEnumerator movePlayer (Vector3 target, float duration, GameObject sourceRoom)
				{
						PlayerController player = PlayerController.Player;
						float t = 0.0f;
						Vector3 startingPos = player.transform.position;
						while (t < 1.0f && !startingPos.Equals(target)) {
								t += Time.deltaTime * (Time.timeScale / duration);
				
								player.transform.position = Vector3.Lerp (startingPos, target, t);
								yield return 0;

						}
						//Activaos las puertas una vez el player ya esta colocado
						sourceRoom.GetComponent<RoomManager> ().reEnableDoors ();
						enableDoors ();
				}



				//Funcion que "activa" la sala a la que nos movemos. Mueve la camara y el personaje
				// "desactiva" las puertas para que podamos atravesarlas
				// Y activa los demas elementos como enemigos.
				public void activateRoom (Directions source)
				{
						Vector3 cameraTarget;
						Vector3 playerTarget;
						GameObject sourceRoom;

						float transitionTime = 1;
						float distanceToDoor = 5;
						PlayerController player = PlayerController.Player;

						cameraTarget = this.transform.position;
						cameraTarget.z = Camera.main.transform.position.z;

						switch (source) {
						case Directions.North:
								playerTarget = doorN.transform.position;
								playerTarget.y -= distanceToDoor;
			
								sourceRoom = roomN;


								break;
						case Directions.South:
								playerTarget = doorS.transform.position;
								playerTarget.y += distanceToDoor;
								sourceRoom = roomS;

				
								break;
						case Directions.East:
								playerTarget = doorE.transform.position;
								playerTarget.x -= distanceToDoor;
								sourceRoom = roomE;

				
								break;
						case Directions.West:
								playerTarget = doorW.transform.position;
								playerTarget.x += distanceToDoor;
								sourceRoom = roomW;

				
								break;
						default:
								Debug.Log ("huehue");
								playerTarget = this.transform.position;
								sourceRoom = roomN;
								break;
						}
						playerTarget.z = player.transform.position.z;


						disableDoors ();
						StartCoroutine (movePlayer (playerTarget, transitionTime, sourceRoom));
						StartCoroutine (moveCamera (cameraTarget, transitionTime));
						StartCoroutine (waitTest (2));
						//Activar enemigos y demas cosas de la sala
						enableEnemies ();
						sourceRoom.GetComponent<RoomManager> ().disableEnemies ();


				}

				public void enableDoors ()
				{
						foreach (GameObject door in doors) {
								door.GetComponent<DoorManager> ().enable ();

						}
				}

				public void reEnableDoors ()
				{
						foreach (GameObject door in doors) {
								door.GetComponent<DoorManager> ().reEnable ();
			
						}
				}
	
				public void disableDoors ()
				{
						foreach (GameObject door in doors) {
								door.GetComponent<DoorManager> ().disable ();
			
						}
		
				}
	
				public void openDoors (Doors type)
				{
						DoorManager dm;
						foreach (GameObject door in doors) {
								dm = door.GetComponent<DoorManager> ();
								if (dm.DoorType.Equals (type)) {
										dm.openDoor ();
								}
			
						}
				}

				public IEnumerator waitTest (float time)
				{
		
						float t = 0.0f;

						while (t < time) {
								t += Time.deltaTime;
								yield return 0;
			
						}
						openDoors (Doors.Enemies);
		
				}

				public void removeExit (Directions exit)
				{
						freeExists.Remove (exit);
				}

				public void enableEnemies ()
				{
						foreach (GameObject enemy in enemyList) {
								enemy.SetActive (true);
						}
				}

				public void disableEnemies ()
				{
						foreach (GameObject enemy in enemyList) {
								enemy.SetActive (false);
						}
				}

				public void notifyEnemyDeath (GameObject enemy)
				{
						enemyList.Remove (enemy);
						if (enemyList.Count <= 0) {
								openDoors (Doors.Enemies);
						}
				}

		}
}