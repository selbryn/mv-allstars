﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Scenario
{
		public class DoorManager : MonoBehaviour
		{

				private Doors doorType; //Tipo de puerta ( Normal, Destruible, Enemigos, Oculta.. )
				public Doors DoorType {
						get {
								return doorType;
						}
						set {
								doorType = value;
						}
				}

				public List<Material>  normalMat, bossMat, shopMat, weaponMat, hiddenMat, brokenMat, enemiesMat; //Materiales correspondientes a los distintos estados de la puerta
				
				private List<Material> activeMaterial; //Material activo de la puerta
				private bool isOpen;
				public bool isBreakable = true;
				public Directions direction; //Direccion de la puerta respecto a la sala

				
				public Directions Direction {
						get {
								return direction;
						}
						set {
								direction = value;
						}
				}

				public	Transform room;//Sala a la que pertenece la puerta

				public Transform Room {
						get {
								return room;
						}
						set {
								room = value;
						}
				}

				private bool isActive = false;
				// Use this for initialization
				void Start ()
				{
	
				}
	
				// Update is called once per frame
				void Update ()
				{
	
				}

				public void setDoor (Doors type)
				{
						this.doorType=type;
						isOpen = true;
						switch (type) {
						case Doors.Normal:
								activeMaterial = normalMat;
								break;
						case Doors.Hidden:
								
								activeMaterial = hiddenMat;
								break;
						case Doors.Boss:
								activeMaterial = bossMat;
								break;
						case Doors.Shop:
								activeMaterial = shopMat;
								break;
						case Doors.Weapon:
								activeMaterial = weaponMat;
								break;
						case Doors.Enemies:
								activeMaterial = enemiesMat;
								isOpen = false;
								break;
						default:
								activeMaterial = normalMat;
								break;
						}
						;
						
						if (isOpen) {
								
								this.renderer.material = activeMaterial [0];
						} else {
								
								this.renderer.material = activeMaterial [1];
						}
				}

				public void openDoor ()
				{
						this.collider2D.enabled = false;
						this.renderer.material = activeMaterial [0];
						
				}

				public void closeDoor ()
				{
						this.collider2D.enabled = true;
						this.renderer.material = activeMaterial [1];
				}
	
				public void enable ()
				{
						this.isActive = true;
						if (isOpen) {
								this.collider2D.enabled = false;
						} else {
								this.collider2D.enabled = true;
						}
				}
	
				public void reEnable ()
				{
						this.collider2D.enabled = true;
						this.isActive = false;
				}
	
				public void disable ()
				{
						this.collider2D.enabled = false;
				}
	
				public void breakDoor ()
				{
						if (isBreakable) {
								isOpen = true;
								this.collider2D.enabled = false;
								this.renderer.material = brokenMat [0];
						}

				}
	
				void OnCollisionEnter2D (Collision2D other)
				{
						if (other.gameObject.tag.Equals ("Player")) {
								if (!isActive) {
										room.GetComponent<RoomManager> ().activateRoom (direction);
								} 
								/*
								if (doorType.Equals (Doors.Key) && player.keysCount > 0) {

										openDoor ();
								}
								*/

						} /*else if (other.gameObject.Equals ("Explosion")) {
								if (isActive) {
										breakDoor ();
								}
						}*/
			
				}
		}


}
